import Ractive  	from 'ractive';
import template  	from './template.html';
import customUtils 	from './utils/utils';

export default Ractive.extend({
        template,
        data:() => {return {
			myData: 'MyLib.name',
			fromUtils: customUtils.name
		}},
		oninit:() => {
			  let tmp = 123;
			  console.log('tmp='+tmp);
		}
})