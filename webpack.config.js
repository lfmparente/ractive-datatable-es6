
var webpack = require('webpack');

module.exports = {
    entry: ['src/index'],
    output: {
        path: __dirname + '/',
        filename: 'ractive-index.js',
        library: 'RactiveIndex',
        libraryTarget: 'umd'
    },
    resolve: {
        root: process.cwd(),
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js', '.styl', '.html'],
    },
    module: {
        loaders: [
					{ test: /(\.html)$/,     loader: 'raw-loader'},
					{ test: /(\.jsx|\.js)$/, loader: 'babel', exclude: /(node_modules|bower_components)/ },
        ],
    }
}

